// require file system and jsdom
import fs from 'fs';

// For jsdom version 10 or higher.
// Require JSDOM Class.
import {JSDOM} from 'jsdom';
import anychartFn from 'anychart/dist/js/anychart-bundle.min.js';
import anychartExportFn from 'anychart-nodejs';

// Create instance of JSDOM.
const jsdom = new JSDOM('<body><div id="container" style="font-family: Serif"></div></body>', {runScripts: 'dangerously'});
// Get window
const window = jsdom.window;

const anychart = anychartFn(window);
const anychartExport = anychartExportFn(anychart);

// create and a chart to the jsdom window.
// chart creating should be called only right after anychart-nodejs module requiring
const chart = anychart.pie([10, 20, 7, 18, 30]);
chart.bounds(0, 0, 800, 600);
chart.container('container');
chart.draw();

// generate JPG image and save it to a file
anychartExport.exportTo(chart, 'jpg').then((image) => {
    fs.writeFile('anychart.jpg', image, (fsWriteError) => {
        if (fsWriteError) {
            console.log(fsWriteError);
        } else {
            console.log('Complete');
        }
    });
}, (generationError) => {
    console.log(generationError);
});
